FROM golang:alpine

RUN apk update && apk add --no-cache bash git openssh

WORKDIR /go/src/bitbucket.org/sulamericaic/ms-monitoramento-cotador-go/

ADD . /go/src/bitbucket.org/sulamericaic/ms-monitoramento-cotador-go/

RUN go get
RUN CGO_ENABLED=0 GOOS=linux go build -ldflags "-s" -a -installsuffix cgo -o app .

#FROM alpine:latest
RUN apk --no-cache add ca-certificates
WORKDIR /root/
#COPY --from=builder /go/src/bitbucket.org/sulamericaic/ms-monitoramento-cotador-go/app .
RUN cp -a /go/src/bitbucket.org/sulamericaic/ms-monitoramento-cotador-go/app .
CMD ["./app"] 