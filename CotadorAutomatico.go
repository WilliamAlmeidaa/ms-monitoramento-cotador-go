package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"strings"

	"github.com/nlopes/slack"
)

type Issue struct {
	ServiceDeskID      string `json:"serviceDeskId"`
	RequestTypeID      string `json:"requestTypeId"`
	RequestFieldValues struct {
		Summary          string `json:"summary"`
		Description      string `json:"description"`
		Customfield27736 struct {
			ID string `json:"id"`
		} `json:"customfield_27736"`
		Customfield28000 struct {
			ID string `json:"id"`
		} `json:"customfield_28000"`
	} `json:"requestFieldValues"`
}

type ResponseIssue struct {
	Expands       []string `json:"_expands"`
	IssueID       string   `json:"issueId"`
	IssueKey      string   `json:"issueKey"`
	RequestTypeID string   `json:"requestTypeId"`
	ServiceDeskID string   `json:"serviceDeskId"`
	CreatedDate   struct {
		Iso8601     string `json:"iso8601"`
		Jira        string `json:"jira"`
		Friendly    string `json:"friendly"`
		EpochMillis int64  `json:"epochMillis"`
	} `json:"createdDate"`
	Reporter struct {
		AccountID    string `json:"accountId"`
		Name         string `json:"name"`
		Key          string `json:"key"`
		EmailAddress string `json:"emailAddress"`
		DisplayName  string `json:"displayName"`
		Active       bool   `json:"active"`
		TimeZone     string `json:"timeZone"`
		Links        struct {
			JiraRest   string `json:"jiraRest"`
			AvatarUrls struct {
				Four8X48  string `json:"48x48"`
				Two4X24   string `json:"24x24"`
				One6X16   string `json:"16x16"`
				Three2X32 string `json:"32x32"`
			} `json:"avatarUrls"`
			Self string `json:"self"`
		} `json:"_links"`
	} `json:"reporter"`
	RequestFieldValues []struct {
		FieldID string `json:"fieldId"`
		Label   string `json:"label"`
		Value   struct {
			Self  string `json:"self"`
			Value string `json:"value"`
			ID    string `json:"id"`
		} `json:"value"`
		RenderedValue struct {
			HTML string `json:"html"`
		} `json:"renderedValue,omitempty"`
	} `json:"requestFieldValues"`
	CurrentStatus struct {
		Status         string `json:"status"`
		StatusCategory string `json:"statusCategory"`
		StatusDate     struct {
			Iso8601     string `json:"iso8601"`
			Jira        string `json:"jira"`
			Friendly    string `json:"friendly"`
			EpochMillis int64  `json:"epochMillis"`
		} `json:"statusDate"`
	} `json:"currentStatus"`
	Links struct {
		JiraRest string `json:"jiraRest"`
		Web      string `json:"web"`
		Self     string `json:"self"`
	} `json:"_links"`
}

type EditFieldJira struct {
	Fields struct {
		Customfield12300 struct {
			ID string `json:"id"`
		} `json:"customfield_12300"`
	} `json:"fields"`
}

func main() {
	fmt.Println("Lendo buscaPropostasFilaGAE....", "\n")
	buscaPropostasFilaGAE()

	fmt.Println("Lendo buscaPropostasParaCancelar....", "\n")
	buscaPropostasParaCancelar()

}

func buscaPropostasFilaGAE() {
	cotadorgae := "Cotador - buscaPropostasFilaGAE"
	resp, err := http.Get("http://s01web105/synccotadorbatch/proposta/buscaPropostasFilaGAE")

	if err != nil {
		print(err.Error())
	}

	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)
	ret := (string(body))
	if !strings.Contains(ret, "OK") {
		fmt.Println(ret)
		CreateIssue(cotadorgae, ret)
	} else {
		fmt.Println("Não existem propostas com erro!", "\n")
	}
}

func buscaPropostasParaCancelar() {
	cotadorcancela := "Cotador - buscaPropostasParaCancelar"
	resp, err := http.Get("http://s01web105/synccotadorbatch/proposta/buscaPropostasParaCancelar")

	if err != nil {
		print(err.Error())
	}

	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)
	ret := (string(body))

	if !strings.Contains(ret, "OK") {
		fmt.Println(ret)
		CreateIssue(cotadorcancela, ret)

	} else {
		fmt.Println("Não existem propostas com erro!")
	}
}

func CreateIssue(sumariotext string, descrip string) (incidente string) {
	var issue Issue

	issue.ServiceDeskID = "8"
	issue.RequestTypeID = "205"
	issue.RequestFieldValues.Summary = sumariotext
	issue.RequestFieldValues.Description = descrip
	issue.RequestFieldValues.Customfield27736.ID = "51708"
	issue.RequestFieldValues.Customfield28000.ID = "53148"

	marshalledIssue, err := json.Marshal(issue)

	if err != nil {
		log.Fatal("Error occured when Marshaling Issue:", err)
	}

	return sendRequest(marshalledIssue, "POST", "")
}

func EditIssue(mensagemslack string, chamado string) (ticket string) {
	fmt.Println("Editar Field Issue ...")
	var editfieldjira EditFieldJira

	editfieldjira.Fields.Customfield12300.ID = "16402"
	marshaleditifield, err := json.Marshal(editfieldjira)

	if err != nil {
		log.Fatal("Error occured when Marshaling Issue:", err)
	}
	return EditFieldSistema(marshaleditifield, chamado, mensagemslack)
}

func sendRequest(jsonStr []byte, method string, url string) (incidente string) {
	req, err := http.NewRequest(method, "https://sulamerica.atlassian.net/rest/servicedeskapi/request", bytes.NewBuffer(jsonStr))
	req.Header.Set("Content-Type", "application/json")
	req.SetBasicAuth("sustentacao.saude@sulamerica.com.br", "3UePaLwxD9K1rzARPSYP0E7A")

	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		panic(err)
	}
	fmt.Println(string(jsonStr))
	body, _ := ioutil.ReadAll(resp.Body)
	var responseIssue ResponseIssue
	json.Unmarshal(body, &responseIssue)
	var resultado = (responseIssue.IssueKey)
	site := ("https://sulamerica.atlassian.net/browse/")
	str := fmt.Sprint("O chamado é: ", site+resultado)
	EditIssue(str, resultado)
	return (resp.Status)

}

func EditFieldSistema(jsonStr []byte, ticket string, msgslack string) (incidente string) {

	req, err := http.NewRequest("PUT", "https://sulamerica.atlassian.net/rest/api/2/issue/"+ticket, bytes.NewBuffer(jsonStr))
	req.Header.Set("Content-Type", "application/json")
	req.SetBasicAuth("sustentacao.saude@sulamerica.com.br", "3UePaLwxD9K1rzARPSYP0E7A")

	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		panic(err)
	}
	sendSlack(msgslack)
	return (resp.Status)
}

func sendSlack(test string) {

	api := slack.New("xoxp-63252432834-286839305584-484901858737-37452c60497b240b5e6f20d1daa0bcdd")
	attachment := slack.Attachment{
		Pretext: "Propostas do Cotador com erro, chamado aberto automáticamente.",
		Text:    test,
	}
	channelID, timestamp, err := api.PostMessage("Monitoramento-susis", slack.MsgOptionAttachments(attachment))
	if err != nil {
		fmt.Printf("%s\n", err)
		return
	}
	fmt.Printf("Message successfully sent to channel %s at %s", channelID, timestamp)
}
